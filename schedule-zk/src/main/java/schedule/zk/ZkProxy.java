package schedule.zk;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author liwenguang
 * @Date 2018/4/28 下午4:36
 * @Description
 */
@Slf4j
@Component("ZkProxy")
public class ZkProxy {

    @Autowired
    private ZkClient zkClient;


    public void start(){
        zkClient.start();
    }

    public String getData(String data){
        return zkClient.getString(data);
    }
}