import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import schedule.zk.ZkClient;
import schedule.zk.ZkProxy;

/**
 * @Author liwenguang
 * @Date 2018/4/28 下午6:34
 * @Description
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class TestZkProxy {

    @Autowired
    private ZkProxy zkProxy;

    @Test
    public void start() {
        zkProxy.start();
        String z = zkProxy.getData("/test");
        System.out.println(z);
    }
}