package schedule.zk.exception;

/**
 * @Author liwenguang
 * @Date 2018/4/28 下午5:46
 * @Description
 */
public class ZkException extends RuntimeException {

    private static final long serialVersionUID = -23123290745766939L;

    private String ZkCode;
    private String ZkMessage;

    public ZkException(String ZkMessage) {
        this("001", ZkMessage);
    }

    public ZkException(Throwable throwable) {
        super(throwable);
        this.ZkCode = "001";
        this.ZkMessage = throwable.getMessage();
    }

    public ZkException(String ZkCode, String ZkMessage) {
        super(ZkMessage);
        this.ZkCode = ZkCode;
        this.ZkMessage = ZkMessage;
    }

    public String getZkCode() {
        return ZkCode;
    }

    public String getZkMessage() {
        return ZkMessage;
    }
}